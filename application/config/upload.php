$config = array(
'upload_path' => "./uploads/",
'allowed_types' => "*",
'overwrite' => TRUE,
'max_size' => "1024000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
'max_height' => "600",
'max_width' => "600"
);