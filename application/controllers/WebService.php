<?php
//error_reporting(0);
//ini_set('display_errors', 0);

defined('BASEPATH') OR exit('No direct script access allowed');

class WebService extends CI_Controller {
    
    const API_SECRET = "kdw9e89wfv9djug9sdfdw38s3";
    var $error_val = array();
        
    public function index()
	{
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $this->api_secret = $this->input->get('apisec', TRUE);
        $running["running"] = "ok";
        echo json_encode($running);
        exit();
    }
    
    public function load($api_secret, $id_empresa = "", $legajo = "")
	{
        if(!isset($api_secret) ||
          empty($id_empresa) ||
          empty($legajo)) {
            echo json_encode($this->error_val);
            exit();
        }
        
        if(!$this->checkApiSecret($api_secret)) {
            echo json_encode($this->error);
            exit();
        }
        
        $this->load->model('WebServiceModel');
        $empleado = $this->WebServiceModel->get_empleado($legajo);
        echo json_encode($empleado);
        exit();
	}
    
    public function login($api_secret, $id_empresa = "", $legajo = "", $clave = "")
	{   
        if(!isset($api_secret) ||
          empty($id_empresa) ||
          empty($legajo) ||
          empty($clave)) {
            echo json_encode($this->error_val);
            exit();
        }
        
        if(!$this->checkApiSecret($api_secret)) {
            echo json_encode($this->error_val);
            exit();
        }
        
        $this->load->model('WebServiceModel');
        $empleado = $this->WebServiceModel->get_empleado_clave($legajo, $clave);
        if(count($empleado) > 0) {
            $empleado[0]->FECHA = time();
        }
        echo json_encode($empleado);
        exit();
	}
    
    public function save($api_secret, $id_marcacion = "", $legajo = "", $clave = "", $tipo_ingreso = "", $fecha = "", $fecha_servidor = "", $imagen = "", $latitud = "", $longitud = "", $dispositivo = "")
	{   
        if(!isset($api_secret) ||
          empty($id_marcacion) ||
          empty($legajo) ||
          empty($tipo_ingreso) ||
          empty($fecha)) {
            echo json_encode($this->error_val);
            exit();
        }
        
        if(!$this->checkApiSecret($api_secret)) {
            echo json_encode($this->error_val);
            exit();
        }

        if($tipo_ingreso === "false") {
            $tipo_ingreso = false;
        } else {
            $tipo_ingreso = true;
        }
        
        $this->load->model('WebServiceModel');
        $empleado = $this->WebServiceModel->guardar_marcacion($id_marcacion, $legajo, $clave, $tipo_ingreso, $fecha, $fecha_servidor, $imagen, $latitud, $longitud, $dispositivo);
        echo json_encode(array('marcacion' => array('estado' =>'OK')));
        exit();
	}

    public function save_image()
    {
        $api_secret = $this->input->post('api_secret');
        
        /*
        if(!isset($this->input->post('name'))) {
            echo json_encode($this->error_val);
            exit();
        }
        */
        
        if(!$this->checkApiSecret($api_secret)) {
            echo json_encode($this->error_val);
            exit();
        }

        $config = array(
        'upload_path' => "./uploads/",
        'allowed_types' => "*",
        'overwrite' => TRUE,
        'max_size' => "1024000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
        'max_height' => "600",
        'max_width' => "600"
        );
        
        $this->load->library('upload', $config);
        //$this->upload->initi‌​alize($config);
        
        if($this->upload->do_upload('userfile'))
        {
            $data = array('upload_data' => $this->upload->data());
            print_r($data);
            //$this->load->view('upload_success',$data);
        }
        else
        {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
            //$this->load->view('file_view', $error);
        }

        echo json_encode(array('imagen_guardada' => 'OK'));
        exit();
    }

    
    private function checkApiSecret($api_secret) {
        if($api_secret === self::API_SECRET) {
            return true;
        } else {
            return false;
        }
    }
    
}
