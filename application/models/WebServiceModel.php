<?php

class WebServiceModel extends CI_Model {

    public $ID_MARCACION;
    public $ID_LEGAJO;
    public $PASSWORD;
    public $ESTADO;
    public $FECHA;
    public $SHOOTFILE;
    public $LAT;
    public $LNG;

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_last_ten_entries()
    {
        $query = $this->db->get('prod_nomina', 10);
        return $query->result();
    }
    
    public function get_empleado($legajo)
    {
        $this->db->where('ID_LEGAJO', $legajo);
        $query = $this->db->get('PROD_NOMINA');
        return $query->result();
    }
    
    public function get_empleado_clave($legajo, $clave)
    {
        $this->db->where('ID_LEGAJO', $legajo);
        $this->db->where('PASSWORD', $clave);
        $query = $this->db->get('SHOOTER_PASSWORDXLEG');
        return $query->result();
    }
    

    public function guardar_marcacion($id_marcacion, $legajo, $clave, $tipo_ingreso, $fecha, $fecha_servidor, $imagen, $latitud, $longitud, $dispositivo)
    {
        $h = 3;
        $hm = $h * 60; 
        $ms = $hm * 60;
        $this->ID_MARCACION = $id_marcacion;
        $this->ID_LEGAJO = $legajo;
        if(!empty($this->PASSWORD)) {
            $this->PASSWORD = $clave;
        }
        $this->ESTADO = $tipo_ingreso;
        $this->FECHA = gmdate('d-m-Y g:i:s A', $fecha - $ms);
        if($tipo_ingreso) {
            $this->FECHA_SERVIDOR = gmdate('d-m-Y g:i:s A', $fecha_servidor - $ms);
        }
        $this->SHOOTFILE = $imagen;
        $this->LAT = $latitud;
        $this->LNG = $longitud;
        $this->ID_EQUIPO = $dispositivo;

        $this->db->insert('MARCACIONES', $this);
    }
    /*
    public function update_entry()
    {
        $this->title    = $_POST['title'];
        $this->content  = $_POST['content'];
        $this->date     = time();

        $this->db->update('legajos', $this, array('id' => $_POST['id']));
    }
    */

}

?>